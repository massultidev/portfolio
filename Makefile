# Copyright by: P.J. Grochowski
DIR_MAKEFILE:=$(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))

DIR_BUILD="$(DIR_MAKEFILE)/build"
DIR_DEPLOY="$(DIR_MAKEFILE)/deploy"

deploy:
	cd "$(DIR_MAKEFILE)" && \
	yarn build && \
	rm -rfv "$(DIR_DEPLOY)"/* && \
	cp -rfv "$(DIR_BUILD)"/. "$(DIR_DEPLOY)" && \
	cd "$(DIR_DEPLOY)" && \
	git add . && \
	git commit -m "Deploy." && \
	git push

.PHONY: deploy
