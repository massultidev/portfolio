// Copyright by: P.J. Grochowski
import "./Header.scss";
import { FC, useEffect, useState } from "react";
import BEMHelper from "react-bem-helper";
import { v4 as uuid } from "uuid";
import { useServicesContext } from "src/common/service/ServicesProvider";
import { DeviceType } from "src/common/service/DeviceTypeObserver";
import { ScrollDirection } from "src/common/service/PageScrollObserver";
import MenuButton from "./MenuButton";
import HeaderLogo from "./HeaderLogo";
import NavItem from "./NavItem";

const classes = new BEMHelper("Header");

const ELEMENT_NAV = "nav";

const Header: FC = () => {
  const {
    fragmentRegistryService,
    outsideClickObserver,
    deviceTypeObserver,
    pageScrollObserver,
  } = useServicesContext();

  const [fragmentsMap, setFragmentsMap] = useState(() => new Map());
  const [isMenuVisible, setIsMenuVisible] = useState(false);
  const [deviceType, setDeviceType] = useState(deviceTypeObserver.deviceType);
  const [isNavbarVisible, setIsNavbarVisible] = useState(true);

  const onMenuVisibility = (state: boolean) => {
    setIsMenuVisible(state);
    if (!state) {
      pageScrollObserver.forceNextUpdate();
    }
  };

  useEffect(() => {
    fragmentRegistryService.subscribe(setFragmentsMap);

    return () => fragmentRegistryService.unsubscribe(setFragmentsMap);
  }, [fragmentRegistryService]);

  useEffect(() => {
    if (!isMenuVisible) return;

    const selector = `.${classes(ELEMENT_NAV).className}`;

    outsideClickObserver.subscribe(selector, () => onMenuVisibility(false));

    return () => outsideClickObserver.unsubscribe(selector);
  });

  useEffect(() => {
    deviceTypeObserver.subscribe(setDeviceType);

    return () => deviceTypeObserver.unsubscribe(setDeviceType);
  }, [deviceTypeObserver]);

  useEffect(() => {
    if (isMenuVisible || deviceType !== DeviceType.Mobile) return;

    const onScroll = (direction: ScrollDirection) =>
      setIsNavbarVisible(direction === ScrollDirection.Up);

    pageScrollObserver.subscribe(onScroll);

    return () => pageScrollObserver.unsubscribe(onScroll);
  }, [pageScrollObserver, isMenuVisible, deviceType]);

  return (
    <header id="#">
      <nav {...classes(ELEMENT_NAV, { hidden: !isNavbarVisible })}>
        <HeaderLogo label="Portfolio" link="/#" />
        <span {...classes("item-container")}>
          <span {...classes("item-list", { visible: isMenuVisible })}>
            {Array.from(fragmentsMap).map(([link, label]) => (
              <NavItem key={uuid()} label={label} link={`/#${link}`} />
            ))}
          </span>
        </span>
        <MenuButton
          {...classes("menu-button")}
          isActive={isMenuVisible}
          onClick={onMenuVisibility}
        />
      </nav>
    </header>
  );
};

export default Header;
