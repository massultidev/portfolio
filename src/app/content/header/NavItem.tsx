// Copyright by: P.J. Grochowski
import "./NavItem.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("NavItem");

interface Props {
  label: string;
  link: string;
}

const NavItem: FC<Props> = ({ label, link }) => {
  return (
    <span {...classes("container")}>
      <a {...classes("button")} href={link}>
        <span>{label}</span>
      </a>
    </span>
  );
};

export default NavItem;
