// Copyright by: P.J. Grochowski
import "./MenuButton.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("MenuButton");

interface Props {
  className?: string;
  isActive?: boolean;
  onClick?: (isActive: boolean) => void;
}

const MenuButton: FC<Props> = ({
  className: classNames = "",
  isActive = false,
  onClick = () => {},
}) => {
  return (
    <span {...classes("container", {}, classNames)}>
      <div
        {...classes("button", { active: isActive })}
        onClick={() => onClick(!isActive)}
      >
        <div {...classes("bar1")} />
        <div {...classes("bar2")} />
        <div {...classes("bar3")} />
      </div>
    </span>
  );
};

export default MenuButton;
