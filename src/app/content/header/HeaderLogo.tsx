// Copyright by: P.J. Grochowski
import "./HeaderLogo.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("HeaderLogo");

interface Props {
  label: string;
  link?: string;
}

const HeaderLogo: FC<Props> = ({ label, link }) => {
  return (
    <a {...classes("container")} href={link}>
      <span>{label}</span>
    </a>
  );
};

export default HeaderLogo;
