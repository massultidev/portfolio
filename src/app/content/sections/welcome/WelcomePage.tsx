// Copyright by: P.J. Grochowski
import "./WelcomePage.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import Typewriter from "src/common/component/Typewriter";

const classes = new BEMHelper("WelcomePage");

const DIR_ASSETS = "/assets/welcome/";
const ROLES = ["Hobbyst", "Programmer", "Developer", "Engineer"];

const WelcomePage: FC = () => {
  return (
    <div {...classes("container")}>
      <div {...classes("canvas")}>
        <picture>
          <source
            media="(min-width: 960px)"
            srcSet={DIR_ASSETS + "bkg-large.jpg"}
          />
          <source
            media="(min-width: 600px)"
            srcSet={DIR_ASSETS + "bkg-medium.jpg"}
          />
          <source srcSet={DIR_ASSETS + "bkg-small.jpg"} />
          <img
            {...classes("image")}
            src={DIR_ASSETS + "bkg-large.jpg"}
            alt="Background missing..."
          />
        </picture>

        <div {...classes("content")}>
          <h2>Welcome to my portfolio!</h2>
          <Typewriter {...classes("type-writer")} texts={ROLES} />
        </div>
      </div>
    </div>
  );
};

export default WelcomePage;
