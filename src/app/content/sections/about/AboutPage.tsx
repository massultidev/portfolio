// Copyright by: P.J. Grochowski
import "./AboutPage.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import { useFragmentRegistration } from "src/common/service";

const classes = new BEMHelper("AboutPage");

const TITLE = "About";
const SECTION_ID = "about";

const AboutPage: FC = () => {
  useFragmentRegistration(SECTION_ID, TITLE);

  return (
    <section {...classes("container")} id={SECTION_ID}>
      <h1>{TITLE}</h1>
      <div {...classes("content")}>
        <div {...classes("portrait")}>
          <img src="/assets/about/portrait.jpg" alt="Portrait missing..." />
        </div>
        <div {...classes("about")}>
          I'm a graduate of Wroclaw University of Science and Technology.
          Software development is not only my profession but above all my
          passion. Always looking for opportunities to improve myself.
        </div>
      </div>
    </section>
  );
};

export default AboutPage;
