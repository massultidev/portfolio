// Copyright by: P.J. Grochowski
import certificateDetailsRaw from "./certificate-details.json";

interface CertificateDetailsRaw {
  icon: string;
  name: string;
  issuer: string;
  url?: string;
  badge?: string;
}

const ASSETS_DIR = "/assets/certificates";

export default class CertificateDetails {
  public readonly iconPath: string;
  public readonly name: string;
  public readonly issuer: string;
  public readonly credentialsUrl?: string;
  public readonly badgePath?: string;

  constructor(detailsRaw: CertificateDetailsRaw) {
    this.iconPath = `${ASSETS_DIR}/${detailsRaw.icon}`;
    this.name = detailsRaw.name;
    this.issuer = detailsRaw.issuer;
    this.credentialsUrl = detailsRaw.url;
    this.badgePath =
      detailsRaw.badge != null
        ? `${ASSETS_DIR}/badge/${detailsRaw.badge}`
        : undefined;
  }
}

export const certificates = (
  certificateDetailsRaw as CertificateDetailsRaw[]
).map((detailsRaw) => new CertificateDetails(detailsRaw));
