// Copyright by: P.J. Grochowski
import "./CertificatesPage.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import { useFragmentRegistration } from "src/common/service";
import CertificatesRow from "./CertificatesRow";
import { v4 as uuid } from "uuid";
import { certificates } from "./CertificateDetails";
import Image from "src/common/component/Image";

const classes = new BEMHelper("CertificatesPage");

const TITLE = "Certificates";
const SECTION_ID = "certificates";

const CertificatesPage: FC = () => {
  useFragmentRegistration(SECTION_ID, TITLE);

  return (
    <section {...classes("container")} id={SECTION_ID}>
      <h1>{TITLE}</h1>
      <div {...classes("content")}>
        <div {...classes("certificate-list")}>
          {certificates.map((certificateDetails) => (
            <CertificatesRow
              key={uuid()}
              certificateDetails={certificateDetails}
            />
          ))}
        </div>
        <div {...classes("badge-list")}>
          {certificates.map(
            (cert) =>
              cert.badgePath && (
                <Image key={uuid()} src={cert.badgePath} maxWidth="150px" />
              )
          )}
        </div>
      </div>
    </section>
  );
};

export default CertificatesPage;
