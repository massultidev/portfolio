// Copyright by: P.J. Grochowski
import "./CertificatesRow.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import CertificateDetails from "./CertificateDetails";
import Link from "src/common/component/Link";

const classes = new BEMHelper("CertificatesRow");

interface Props {
  certificateDetails: CertificateDetails;
}

const CertificatesRow: FC<Props> = ({
  certificateDetails: { iconPath, name, issuer, credentialsUrl },
}) => {
  return (
    <div {...classes("container")}>
      <div {...classes("icon")}>
        <img src={iconPath} alt="Icon missing..." />
      </div>
      <div {...classes("content")}>
        <p {...classes("title")}>{name}</p>
        <p {...classes("issuer")}>Issuer: {issuer}</p>
        {credentialsUrl && <Link label="Credentials" url={credentialsUrl} />}
      </div>
    </div>
  );
};

export default CertificatesRow;
