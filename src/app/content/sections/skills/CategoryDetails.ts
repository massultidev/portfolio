// Copyright by: P.J. Grochowski
import categoryDetails from "./category-details.json";

export default interface CategoryDetails {
  readonly category: string;
  readonly technologies: string;
}

export const categories = categoryDetails as CategoryDetails[];
