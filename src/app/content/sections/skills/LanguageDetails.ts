// Copyright by: P.J. Grochowski
import languageDetails from "./language-details.json";

interface LanguageDetailsRaw {
  icon: string;
  name: string;
  frameworks: string;
}

const ASSETS_DIR = "/assets/skills";

export default class LanguageDetails {
  readonly iconPath: string;
  readonly name: string;
  readonly frameworks: string;

  constructor(detailsRaw: LanguageDetailsRaw) {
    this.iconPath = `${ASSETS_DIR}/${detailsRaw.icon}`;
    this.name = detailsRaw.name;
    this.frameworks = detailsRaw.frameworks;
  }
}

export const languages = (languageDetails as LanguageDetailsRaw[]).map(
  (detailsRaw) => new LanguageDetails(detailsRaw)
);
