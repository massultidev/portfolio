// Copyright by: P.J. Grochowski
import "./SkillsRow.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import LanguageDetails from "./LanguageDetails";

const classes = new BEMHelper("SkillsRow");

interface Props {
  languageDetails: LanguageDetails;
}

const SkillsRow: FC<Props> = ({
  languageDetails: { iconPath, name, frameworks },
}) => {
  return (
    <div {...classes("container")}>
      <div {...classes("icon")}>
        <img src={iconPath} alt="Icon missing..." />
      </div>
      <div {...classes("content")}>
        <p {...classes("title")}>{name}</p>
        <p>{frameworks}</p>
      </div>
    </div>
  );
};

export default SkillsRow;
