// Copyright by: P.J. Grochowski
import "./SkillsPage.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import { useFragmentRegistration } from "src/common/service";
import SkillsRow from "./SkillsRow";
import { v4 as uuid } from "uuid";
import { categories } from "./CategoryDetails";
import { languages } from "./LanguageDetails";

const classes = new BEMHelper("SkillsPage");

const TITLE = "Skills";
const SECTION_ID = "skills";

const SkillsPage: FC = () => {
  useFragmentRegistration(SECTION_ID, TITLE);

  return (
    <section {...classes("container")} id={SECTION_ID}>
      <h1>{TITLE}</h1>
      <div {...classes("content")}>
        <div {...classes("technologies-table")}>
          <table>
            <thead>
              <tr>
                <th>Category</th>
                <th>Technologies</th>
              </tr>
            </thead>
            <tbody>
              {categories.map(({ category, technologies }) => (
                <tr key={uuid()}>
                  <td>{category}</td>
                  <td>{technologies}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div {...classes("frameworks-list")}>
          {languages.map((languageDetails) => (
            <SkillsRow key={uuid()} languageDetails={languageDetails} />
          ))}
        </div>
      </div>
    </section>
  );
};

export default SkillsPage;
