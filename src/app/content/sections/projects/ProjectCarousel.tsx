// Copyright by: P.J. Grochowski
import "./ProjectCarousel.scss";
import { FC, PropsWithChildren } from "react";
import BEMHelper from "react-bem-helper";
import { v4 as uuid } from "uuid";
import { Carousel, Image } from "src/common/component";
import ProjectDetails from "./ProjectDetails";

const classes = new BEMHelper("ProjectCarousel");

interface Props {
  project: ProjectDetails;
}

const ProjectCarousel: FC<PropsWithChildren<Props>> = ({
  project: { assetsDir, assets },
}) => {
  return (
    <div {...classes("container")}>
      <Carousel>
        {assets.map((asset) => (
          <Image
            key={uuid()}
            src={`${assetsDir}/${asset}`}
            alt={`Asset "${asset}" missing...`}
          />
        ))}
      </Carousel>
    </div>
  );
};

export default ProjectCarousel;
