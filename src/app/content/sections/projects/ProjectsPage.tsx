// Copyright by: P.J. Grochowski
import "./ProjectsPage.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import { v4 as uuid } from "uuid";
import { useFragmentRegistration } from "src/common/service";
import ProjectCarousel from "./ProjectCarousel";
import ProjectDetailsPanel from "./ProjectDetailsPanel";
import { projects } from "./ProjectDetails";

const classes = new BEMHelper("ProjectsPage");

const TITLE = "Projects";
const SECTION_ID = "projects";

const ProjectsPage: FC = () => {
  useFragmentRegistration(SECTION_ID, TITLE);

  return (
    <section {...classes("container")} id={SECTION_ID}>
      <h1>{TITLE}</h1>
      <div {...classes("content")}>
        {projects.map((project) => (
          <div key={uuid()} {...classes("project")}>
            <div {...classes("carousel")}>
              <ProjectCarousel project={project} />
            </div>
            <div {...classes("details")}>
              <ProjectDetailsPanel project={project} />
            </div>
          </div>
        ))}
      </div>
    </section>
  );
};

export default ProjectsPage;
