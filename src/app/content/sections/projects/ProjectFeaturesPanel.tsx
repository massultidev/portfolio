// Copyright by: P.J. Grochowski
import "./ProjectFeaturesPanel.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import { v4 as uuid } from "uuid";

const classes = new BEMHelper("ProjectFeaturesPanel");

interface Props {
  featuresList: string[];
}

const ProjectFeaturesPanel: FC<Props> = ({ featuresList }) => {
  return (
    <div {...classes("container")}>
      <label {...classes("title")}>Features:</label>
      <ul {...classes("list")}>
        {featuresList.map((feature) => (
          <li key={uuid()} {...classes("item")}>
            {feature}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ProjectFeaturesPanel;
