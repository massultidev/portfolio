// Copyright by: P.J. Grochowski
import "./ProjectDetailsPanel.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import ProjectDetails, { ReleaseStatus } from "./ProjectDetails";
import Tag, { TagVariant } from "src/common/component/Tag";
import Link from "src/common/component/Link";
import ProjectFeaturesPanel from "./ProjectFeaturesPanel";

const classes = new BEMHelper("ProjectDetailsPanel");

interface Props {
  project: ProjectDetails;
}

const ProjectDetailsPanel: FC<Props> = ({
  project: {
    name,
    status,
    description,
    featuresList,
    techstack,
    assetsDir,
    downloadUrl,
  },
}) => {
  const getTagVariant = (status: ReleaseStatus) => {
    return status === ReleaseStatus.RELEASED
      ? TagVariant.Good
      : TagVariant.Warning;
  };

  return (
    <div {...classes("container")}>
      <div {...classes("header")}>
        <div {...classes("icon")}>
          <img src={`${assetsDir}/icon.png`} alt="Icon missing..." />
        </div>
        <div {...classes("details")}>
          <div {...classes("title-and-status")}>
            <p>{name}</p>
            <Tag variant={getTagVariant(status)}>{status}</Tag>
          </div>
          <p>{techstack}</p>
          {downloadUrl && <Link label="Download" url={downloadUrl} />}
        </div>
      </div>
      <div {...classes("description")}>
        <p>{description}</p>
        {Array.isArray(featuresList) && !!featuresList.length && (
          <ProjectFeaturesPanel featuresList={featuresList} />
        )}
      </div>
    </div>
  );
};

export default ProjectDetailsPanel;
