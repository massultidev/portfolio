// Copyright by: P.J. Grochowski
import projectDetails from "./project-details.json";

interface ProjectDetailsRaw {
  name: string;
  released: boolean;
  techstack: string;
  description: string[];
  features: string[];
  assetsDir: string;
  assetsCount: number;
  downloadUrl?: string;
}

const ASSETS_DIR = "/assets/projects";

export enum ReleaseStatus {
  RELEASED = "Released",
  IN_DEVELOPMENT = "In Development",
}

export default class ProjectDetails {
  readonly name: string;
  readonly status: ReleaseStatus;
  readonly techstack: string;
  readonly description: string;
  readonly featuresList: string[];
  readonly assetsDir: string;
  readonly assets: string[];
  readonly downloadUrl?: string;

  constructor(detailsRaw: ProjectDetailsRaw) {
    this.name = detailsRaw.name;
    this.status = detailsRaw.released
      ? ReleaseStatus.RELEASED
      : ReleaseStatus.IN_DEVELOPMENT;
    this.techstack = detailsRaw.techstack;
    this.description = detailsRaw.description.join("\n");
    this.featuresList = detailsRaw.features;
    this.assetsDir = `${ASSETS_DIR}/${detailsRaw.assetsDir}`;
    this.assets = Array(detailsRaw.assetsCount)
      .fill(null)
      .map((item, i) => `img-${i + 1}.png`);
    this.downloadUrl = detailsRaw.downloadUrl;
  }
}

export const projects = (projectDetails as ProjectDetailsRaw[]).map(
  (detailsRaw) => new ProjectDetails(detailsRaw)
);
