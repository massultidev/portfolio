// Copyright by: P.J. Grochowski
import moment from "moment";
import jobDetailsRaw from "./job-details.json";

interface JobDetailsRaw {
  since: string;
  until: string | null;
  employer: string;
  position: string;
  techstack: string;
}

const DEFAULT_FORMAT = "YYYY.MM.DD";
const DEFAULT_END_DATE_STR = "Present";

export default class JobDetails {
  public readonly since: Date;
  public readonly until: Date | null;
  public readonly employer: string;
  public readonly position: string;
  public readonly techstack: string;

  constructor(rawDetails: JobDetailsRaw) {
    this.since = new Date(rawDetails.since);
    this.until = rawDetails.until != null ? new Date(rawDetails.until) : null;
    this.employer = rawDetails.employer;
    this.position = rawDetails.position;
    this.techstack = rawDetails.techstack;
  }

  get sinceFormatted() {
    return this.formatDate(this.since);
  }

  get untilFormatted() {
    return this.until === null
      ? DEFAULT_END_DATE_STR
      : this.formatDate(this.until);
  }

  get durationFormatted() {
    const monthDiff = this.monthDiff();
    const months = monthDiff % 12;
    const years = Math.floor(monthDiff / 12);
    return `(Years: ${years} & Months: ${months})`;
  }

  hasEnded() {
    return this.until !== null;
  }

  private formatDate(date: Date) {
    return moment(date).format(DEFAULT_FORMAT);
  }

  private monthDiff() {
    const until = this.until === null ? new Date() : this.until;
    return (
      Math.round((until.getDate() - this.since.getDate()) / 30) +
      until.getMonth() -
      this.since.getMonth() +
      12 * (until.getFullYear() - this.since.getFullYear())
    );
  }
}

export const jobs = (jobDetailsRaw as JobDetailsRaw[]).map(
  (rawDetails) => new JobDetails(rawDetails)
);
