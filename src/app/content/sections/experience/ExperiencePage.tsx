// Copyright by: P.J. Grochowski
import "./ExperiencePage.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import { useFragmentRegistration } from "src/common/service";
import ExperienceRow from "./ExperienceRow";
import { v4 as uuid } from "uuid";
import { jobs } from "./JobDetails";

const classes = new BEMHelper("ExperiancePage");

const TITLE = "Experience";
const SECTION_ID = "experience";

const ExperiancePage: FC = () => {
  useFragmentRegistration(SECTION_ID, TITLE);

  return (
    <section {...classes("container")} id={SECTION_ID}>
      <h1>{TITLE}</h1>
      <ul {...classes("timeline")}>
        {jobs.map((job) => (
          <ExperienceRow key={uuid()} jobDetails={job} />
        ))}
      </ul>
    </section>
  );
};

export default ExperiancePage;
