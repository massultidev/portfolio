// Copyright by: P.J. Grochowski
import "./ExperienceRow.scss";
import { FC } from "react";
import JobDetails from "./JobDetails";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("ExperienceRow");

interface Props {
  jobDetails: JobDetails;
}

const ExperienceRow: FC<Props> = ({ jobDetails }) => {
  const durationSummary = `${jobDetails.sinceFormatted} - ${jobDetails.untilFormatted}\n${jobDetails.durationFormatted}`;

  return (
    <li
      {...classes("timeline-event", { incomplete: !jobDetails.hasEnded() })}
      data-when={durationSummary}
    >
      <h3>{jobDetails.position}</h3>
      <p>
        <i>{jobDetails.employer}</i>
      </p>
      <p>{jobDetails.techstack}</p>
    </li>
  );
};

export default ExperienceRow;
