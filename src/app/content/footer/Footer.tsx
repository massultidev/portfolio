// Copyright by: P.J. Grochowski
import "./Footer.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";
import IconButton from "src/common/component/IconButton";
import { iconButtonDetails } from "./IconButtonDetails";
import { v4 as uuid } from "uuid";

const classes = new BEMHelper("Footer");

const Footer: FC = () => {
  return (
    <footer>
      <div {...classes("container")}>
        <div {...classes("left")}>
          <span {...classes("copyright")}>Copyright by: P.J.Grochowski</span>
        </div>
        <div {...classes("right")}>
          {iconButtonDetails.map((details) => (
            <IconButton
              key={uuid()}
              icon={details.iconUrl}
              href={details.targetUrl}
              title={details.title}
            />
          ))}
        </div>
      </div>
    </footer>
  );
};

export default Footer;
