// Copyright by: P.J. Grochowski

const DIR_ASSETS = "/assets/footer/";

export default class IconButtonDetails {
  public readonly iconUrl;

  constructor(
    iconFileName: string,
    public readonly targetUrl: string,
    public readonly title: string
  ) {
    this.iconUrl = DIR_ASSETS + iconFileName;
  }
}

export const iconButtonDetails = [
  new IconButtonDetails(
    "icon-linkedin.png",
    "https://www.linkedin.com/in/pawe%C5%82-grochowski-68b37916b/",
    "linkedin"
  ),
  new IconButtonDetails(
    "icon-git.png",
    "https://bitbucket.org/massultidev/",
    "git"
  ),
];
