// Copyright by: P.J. Grochowski
import "./App.scss";
import { FC } from "react";
import Container from "src/common/component/Container";
import Header from "./content/header/Header";
import Footer from "./content/footer/Footer";
import WelcomePage from "./content/sections/welcome/WelcomePage";
import ExperiancePage from "./content/sections/experience/ExperiencePage";
import ProjectsPage from "./content/sections/projects/ProjectsPage";
import SkillsPage from "./content/sections/skills/SkillsPage";
import AboutPage from "./content/sections/about/AboutPage";
import CertificatesPage from "./content/sections/certificates/CertificatesPage";
import ServicesProvider from "src/common/service/ServicesProvider";

const App: FC = () => {
  return (
    <ServicesProvider>
      <Header />
      <WelcomePage />
      <Container>
        <ProjectsPage />
        <ExperiancePage />
        <SkillsPage />
        <CertificatesPage />
        <AboutPage />
      </Container>
      <Footer />
    </ServicesProvider>
  );
};

export default App;
