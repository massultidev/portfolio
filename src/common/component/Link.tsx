// Copyright by: P.J. Grochowski
import "./Link.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("Link");

interface Props {
  label: string;
  url: string;
}

const Link: FC<Props> = ({ label, url }) => {
  return (
    <a {...classes("link")} target="_blank" rel="noreferrer" href={url}>
      {label}
    </a>
  );
};

export default Link;
