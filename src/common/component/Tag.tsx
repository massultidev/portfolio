// Copyright by: P.J. Grochowski
import "./Tag.scss";
import { FC, PropsWithChildren } from "react";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("Tag");

export enum TagVariant {
  Good = "good",
  Warning = "warning",
}

interface Props {
  variant?: TagVariant;
}

const Tag: FC<PropsWithChildren<Props>> = ({
  children,
  variant = TagVariant.Warning,
}) => {
  return <span {...classes("container", variant)}>{children}</span>;
};

export default Tag;
