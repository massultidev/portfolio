// Copyright by: P.J. Grochowski
import "./Container.scss";
import { FC, PropsWithChildren } from "react";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("Container");

const Container: FC<PropsWithChildren> = ({ children }) => {
  return <div {...classes("container")}>{children}</div>;
};

export default Container;
