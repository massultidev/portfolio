// Copyright by: P.J. Grochowski
import "./IconButton.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("IconButton");

interface Props {
  icon: string;
  href: string;
  title: string;
  alt?: string;
}

const IconButton: FC<Props> = ({ icon, href, title, alt = title }) => {
  return (
    <a
      {...classes("container")}
      target="_blank"
      rel="noreferrer"
      href={href}
      title={title}
    >
      <img src={icon} alt={alt} />
    </a>
  );
};

export default IconButton;
