// Copyright by: P.J. Grochowski
import "./Carousel.scss";
import {
  FC,
  useState,
  useRef,
  MouseEvent,
  TouchEvent,
  Children,
  useEffect,
  PropsWithChildren,
} from "react";
import BEMHelper from "react-bem-helper";
import { useRefFn, useWindowSize } from "src/common/hook";
import CarouselControlService from "./CarouselControlService";
import CarouselItemList from "./CarouselItemList";
import ArrowButton, { ArrowDirection } from "./ArrowButton";

const classes = new BEMHelper("Carousel");

class Coord {
  constructor(public x: number = 0, public y: number = 0) {}

  add(other: Coord) {
    this.x += other.x;
    this.y += other.y;
  }
}

interface Props {
  transitionDelayMs?: number;
}

const Carousel: FC<PropsWithChildren<Props>> = ({
  children,
  transitionDelayMs = 4000,
}) => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [grabbed, setGrabbed] = useState(false);
  const touchBase = useRef<Coord>(new Coord());
  const touchVector = useRef<Coord>(new Coord());

  const control = useRefFn<CarouselControlService>(
    () => new CarouselControlService(setCurrentIndex)
  );

  control.current.itemCount = Children.count(children);

  const windowSize = useWindowSize();

  useEffect(() => {
    !grabbed ? control.current.focus() : control.current.focusCurrent();
  }, [control, grabbed]);
  useEffect(() => control.current.focusCurrent(), [control, windowSize]);
  useEffect(() => control.current.focus(), [control, currentIndex]);

  useEffect(() => {
    if (grabbed) return;
    const timeoutId = setTimeout(() => {
      control.current?.focusNext();
    }, transitionDelayMs);

    return () => clearTimeout(timeoutId);
  }, [transitionDelayMs, control, currentIndex, grabbed]);

  const onGrabStart = (event: MouseEvent) => {
    event.preventDefault();

    startGrab();
  };

  const onGrabEnd = () => {
    touchBase.current = new Coord();
    touchVector.current = new Coord();

    endGrab();
  };

  const onGrabMove = (event: MouseEvent) => {
    if (grabbed) {
      control.current.move(-event.movementX);
    }
  };

  const onTouchStart = (event: TouchEvent) => {
    onTouchCommon(event, (coord) => {
      touchBase.current = coord;
      touchVector.current = new Coord();
    });

    startGrab();
  };

  const onTouchEnd = () => endGrab();

  const onTouchMove = (event: TouchEvent) =>
    onTouchCommon(event, (coord) => {
      const vector = new Coord(
        coord.x - touchBase.current.x,
        coord.y - touchBase.current.y
      );

      touchBase.current = coord;
      touchVector.current.add(vector);

      if (Math.abs(touchVector.current.y) > Math.abs(touchVector.current.x)) {
        return;
      }

      control.current.move(-vector.x);
    });

  const onTouchCommon = (
    event: TouchEvent,
    callback: (coord: Coord) => void
  ) => {
    if (event.touches.length <= 0) return;
    const touch = event.touches[0];
    callback(new Coord(touch.clientX, touch.clientY));
  };

  const startGrab = () => {
    setGrabbed(true);
  };

  const endGrab = () => {
    if (grabbed) {
      setGrabbed(false);
    }
  };

  const onArrowRight = (event: MouseEvent) => control.current.focusNext();
  const onArrowLeft = (event: MouseEvent) => control.current.focusPrevious();

  return (
    <div
      {...classes("container", { grabbed })}
      onMouseDown={onGrabStart}
      onMouseUp={onGrabEnd}
      onMouseLeave={onGrabEnd}
      onMouseMove={onGrabMove}
      onTouchStart={onTouchStart}
      onTouchMove={onTouchMove}
      onTouchEnd={onTouchEnd}
    >
      <ArrowButton direction={ArrowDirection.Left} onClick={onArrowLeft} />
      <ArrowButton direction={ArrowDirection.Right} onClick={onArrowRight} />
      <CarouselItemList control={control.current}>{children}</CarouselItemList>
    </div>
  );
};

export default Carousel;
