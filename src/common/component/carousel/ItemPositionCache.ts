// Copyright by: P.J. Grochowski

enum ShiftDirection {
  Right,
  Left,
}

interface ItemPosition {
  index: number;
  offset: number;
}
class ItemPositionImpl implements ItemPosition {
  constructor(public index: number = 0, public offset: number = 0) {}
}

export type CurrentIndexCallback = (index: number) => void;

export default class ItemPositionCache {
  private _indexOrder: number[] = [];
  private previous: ItemPosition = new ItemPositionImpl();
  private current: ItemPosition = new ItemPositionImpl();

  private onIndexChange: CurrentIndexCallback;

  constructor(callback: CurrentIndexCallback) {
    this.onIndexChange = callback;

    this.adjustItemOrder();
  }

  get previousIndex() {
    return this.previous.index;
  }

  get currentIndex() {
    return this.current.index;
  }

  get offset() {
    return this.previous.offset || this.current.offset;
  }

  get indexOrder() {
    return this._indexOrder;
  }

  set itemCount(count: number) {
    if (count === this._indexOrder.length) return;

    this._indexOrder = Array(count)
      .fill(null)
      .map((item, i) => i);

    this.adjustItemOrder();
  }

  public updateCurrentOffset(delta: number, limit: number | null = null) {
    const offset = this.current.offset + delta;
    this.current.offset =
      limit != null && Math.abs(offset) > Math.abs(limit)
        ? Math.sign(offset) * Math.abs(limit)
        : offset;
  }

  public updateCurrentIndex(treshold: number) {
    if (Math.abs(this.current.offset) < treshold) return;

    this.goTo(this.current.index + Math.sign(this.current.offset));
  }

  public goTo(index: number, instantly: boolean = false) {
    const newIndex = this.adjustNewIndex(index);

    this.previous.index = instantly ? newIndex : this.current.index;
    this.previous.offset = instantly ? 0 : this.current.offset;

    this.current.index = newIndex;
    this.current.offset = 0;

    this.adjustItemOrder();
  }

  private adjustNewIndex(newIndex: number): number {
    const maxValue = Math.max(...this.indexOrder);
    if (newIndex < 0) {
      return maxValue;
    }
    if (newIndex > maxValue) {
      return 0;
    }

    return newIndex;
  }

  private adjustItemOrder() {
    this.shiftItems();
    this.onIndexChange(this.current.index);
  }

  private shiftItems() {
    const direction = this.getRequiredShiftType();
    if (direction == null) {
      return;
    }

    switch (direction) {
      case ShiftDirection.Right:
        this._indexOrder.push(this._indexOrder.shift() as number);
        break;

      case ShiftDirection.Left:
        this._indexOrder.unshift(this._indexOrder.pop() as number);
        break;

      default:
        console.warn(`Unknown shift direction! ID: ${direction}`);
        break;
    }
  }

  private getRequiredShiftType(): ShiftDirection | null {
    if (this._indexOrder.length < 2) {
      return null;
    }

    const pos = this._indexOrder.findIndex(
      (index) => index === this.current.index
    );

    if (pos < 0) {
      return null;
    }

    if (pos <= 1) {
      return ShiftDirection.Left;
    }

    if (pos >= this._indexOrder.length - 2) {
      return ShiftDirection.Right;
    }

    return null;
  }
}
