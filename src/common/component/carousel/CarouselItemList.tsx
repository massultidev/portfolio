// Copyright by: P.J. Grochowski
import "./CarouselItemList.scss";
import { FC, Children, useCallback, PropsWithChildren } from "react";
import BEMHelper from "react-bem-helper";
import { useRefEffect } from "src/common/hook";
import CarouselControlService, {
  ItemMovement,
  ItemTransition,
} from "./CarouselControlService";
import CarouselItem from "./CarouselItem";

const classes = new BEMHelper("CarouselItemList");

interface Props {
  control: CarouselControlService;
}

const CarouselItemList: FC<PropsWithChildren<Props>> = ({
  children,
  control,
}) => {
  const onRefCallback = useCallback(
    (element: HTMLElement) => {
      const calcOffset = (item: HTMLElement) =>
        item.offsetLeft + (item.offsetWidth - element.offsetWidth) / 2;

      const onMove = (movement: ItemMovement) =>
        element.scroll({
          left: calcOffset(movement.element) + movement.offset,
        });

      const onFocus = (transition: ItemTransition) => {
        element.scroll(calcOffset(transition.original) + transition.offset, 0);
        element.scroll({
          behavior: "smooth",
          left: calcOffset(transition.target),
        });
      };

      control.itemListProxy = {
        onMove,
        onFocus,
      };

      return () => (control.itemListProxy = null);
    },
    [control]
  );

  const setRef = useRefEffect(onRefCallback)[1];

  const items = Children.map(children, (child) => child) ?? [];

  return (
    <div
      ref={setRef}
      {...classes("container")}
      onLoad={() => control.focusCurrent()}
    >
      {control.itemIndexOrder.map((i) => (
        <CarouselItem key={i} index={i} control={control}>
          {items[i]}
        </CarouselItem>
      ))}
    </div>
  );
};

export default CarouselItemList;
