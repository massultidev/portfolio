// Copyright by: P.J. Grochowski
import "./ArrowButton.scss";
import { FC, MouseEvent } from "react";
import BEMHelper, { WordSet } from "react-bem-helper";

const classes = new BEMHelper("ArrowButton");

export enum ArrowDirection {
  Left,
  Right,
}

export type OnClickCallback = (event: MouseEvent) => void;

interface Props {
  direction: ArrowDirection;
  onClick: OnClickCallback;
}

const ArrowButton: FC<Props> = ({ direction, onClick }) => {
  const getDirectionCls = (): WordSet => {
    switch (direction) {
      case ArrowDirection.Left:
        return { left: true };
      case ArrowDirection.Right:
        return { right: true };
      default:
        return {};
    }
  };

  const directionCls = getDirectionCls();

  const onMouseDown = (event: MouseEvent) => event.stopPropagation();

  return (
    <div {...classes("container", directionCls)}>
      <div
        {...classes("icon", directionCls)}
        onClick={onClick}
        onMouseDown={onMouseDown}
      />
    </div>
  );
};

export default ArrowButton;
