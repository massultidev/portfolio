// Copyright by: P.J. Grochowski
import ItemPositionCache, { CurrentIndexCallback } from "./ItemPositionCache";

export interface ItemMovement {
  element: HTMLElement;
  offset: number;
}

export interface ItemTransition {
  original: HTMLElement;
  target: HTMLElement;
  offset: number;
}

type MoveCallback = (movement: ItemMovement) => void;
type FocusCallback = (transition: ItemTransition) => void;

interface ItemListProxy {
  onMove: MoveCallback;
  onFocus: FocusCallback;
}

export default class CarouselControlService {
  private itemListApi: ItemListProxy | null = null;
  private itemRegistry: Map<number, HTMLElement> = new Map();
  private cache: ItemPositionCache;

  constructor(onIndexChange: CurrentIndexCallback) {
    this.cache = new ItemPositionCache(onIndexChange);
  }

  public set itemCount(count: number) {
    this.cache.itemCount = count;
  }

  public set itemListProxy(proxy: ItemListProxy | null) {
    this.itemListApi = proxy;
  }

  public get currentIndex() {
    return this.cache.currentIndex;
  }

  public get itemIndexOrder() {
    return this.cache.indexOrder;
  }

  public move(delta: number) {
    const currentItem = this.getCurrentItem();
    if (currentItem != null) {
      this.cache.updateCurrentOffset(delta, currentItem.offsetWidth * 0.75);
      this.itemListApi?.onMove({
        element: currentItem,
        offset: this.cache.offset,
      });
    }
  }

  public focus(index: number | null = null, instantly: boolean = false) {
    if (index != null) {
      this.cache.goTo(index, instantly);
    } else {
      this.updateCurrentItem();
    }

    const previousItem = this.getPreviousItem();
    const currentItem = this.getCurrentItem();
    if (previousItem != null && currentItem != null) {
      this.itemListApi?.onFocus({
        original: previousItem,
        target: currentItem,
        offset: this.cache.offset,
      });
    }
  }

  public focusCurrent() {
    this.focus(this.cache.currentIndex, true);
  }

  public focusNext() {
    this.focus(this.cache.currentIndex + 1);
  }

  public focusPrevious() {
    this.focus(this.cache.currentIndex - 1);
  }

  public registerItem(index: number, element: HTMLElement) {
    this.itemRegistry.set(index, element);
  }

  public unregisterItem(index: number) {
    this.itemRegistry.delete(index);
  }

  private updateCurrentItem() {
    const currentItem = this.getCurrentItem();
    if (currentItem != null) {
      this.cache.updateCurrentIndex(currentItem.offsetWidth / 2);
    }
  }

  private getCurrentItem(): HTMLElement | null {
    return this.getItem(this.cache.currentIndex);
  }

  private getPreviousItem(): HTMLElement | null {
    return this.getItem(this.cache.previousIndex);
  }

  private getItem(index: number): HTMLElement | null {
    return this.itemRegistry.get(index) ?? null;
  }
}
