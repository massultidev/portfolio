// Copyright by: P.J. Grochowski
import "./CarouselItem.scss";
import { FC, PropsWithChildren } from "react";
import BEMHelper from "react-bem-helper";
import { useRefEffect } from "src/common/hook";
import CarouselControlService from "./CarouselControlService";

const classes = new BEMHelper("CarouselItem");

interface Props {
  control: CarouselControlService;
  index: number;
}

const CarouselItem: FC<PropsWithChildren<Props>> = ({
  children,
  control,
  index,
}) => {
  const setRef = useRefEffect((element) => {
    control.registerItem(index, element);

    return () => control.unregisterItem(index);
  })[1];

  const active = index === control.currentIndex;

  return (
    <div ref={setRef} {...classes("container")}>
      <div {...classes("overlay", { active })} />
      {children}
    </div>
  );
};

export default CarouselItem;
