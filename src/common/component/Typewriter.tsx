// Copyright by: P.J. Grochowski
import "./Typewriter.scss";
import { FC, useState, useEffect, useRef } from "react";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("Typewriter");

enum PrintState {
  TYPING,
  TYPING_COMPLETE,
  ERASING,
  ERASING_COMPLETE,
}

const DEFAULT_TEXT = "";
const DEFAULT_PRINT_STATE = PrintState.TYPING;

type TimeoutId = NodeJS.Timeout | null;

interface Props {
  texts?: string[];
  typeSpeedMs?: number;
  erasingSpeedMs?: number;
  typingDoneSleepMs?: number;
  erasingDoneSleepMs?: number;
  className?: string;
}

const Typewriter: FC<Props> = ({
  texts = [],
  typeSpeedMs = 100,
  erasingSpeedMs = 100,
  typingDoneSleepMs = 1000,
  erasingDoneSleepMs = 1000,
  className = "",
}) => {
  const [content, setContent] = useState(DEFAULT_TEXT);

  const printState = useRef(DEFAULT_PRINT_STATE);

  useEffect(() => {
    let timeoutId: TimeoutId = null;

    const getCurrentText = () => (texts.length === 0 ? DEFAULT_TEXT : texts[0]);

    const switchToNextText = () => {
      if (texts.length > 0) {
        texts.push(texts.shift() as string);
      }
    };

    const onTypeCharacter = (delay = typeSpeedMs) => {
      const currentText = getCurrentText();
      if (content.length >= currentText.length) {
        printState.current = PrintState.TYPING_COMPLETE;
        return false;
      }

      const newContent = currentText.slice(0, content.length + 1);
      timeoutId = setTimeout(() => setContent(newContent), delay);
      return true;
    };

    const onEraseCharacter = (delay = erasingSpeedMs) => {
      if (content.length <= 0) {
        switchToNextText();
        printState.current = PrintState.ERASING_COMPLETE;
        return false;
      }

      const newContent = content.slice(0, -1);
      timeoutId = setTimeout(() => setContent(newContent), delay);
      return true;
    };

    const onTypingComplete = () => {
      printState.current = PrintState.ERASING;
      return onEraseCharacter(typingDoneSleepMs);
    };

    const onErasingComplete = () => {
      printState.current = PrintState.TYPING;
      return onTypeCharacter(erasingDoneSleepMs);
    };

    const onContentChange = () => {
      const states = new Map([
        [PrintState.TYPING, onTypeCharacter],
        [PrintState.TYPING_COMPLETE, onTypingComplete],
        [PrintState.ERASING, onEraseCharacter],
        [PrintState.ERASING_COMPLETE, onErasingComplete],
      ]);

      while (!(states.get(printState.current) as Function)());
    };

    onContentChange();

    return () => clearTimeout(timeoutId as NodeJS.Timeout);
  }, [
    content,
    texts,
    typeSpeedMs,
    erasingSpeedMs,
    typingDoneSleepMs,
    erasingDoneSleepMs,
  ]);

  return (
    <div {...classes("container", {}, className)}>
      <span>{content}</span>
    </div>
  );
};

export default Typewriter;
