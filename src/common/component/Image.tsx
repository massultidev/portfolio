// Copyright by: P.J. Grochowski
import "./Image.scss";
import { FC } from "react";
import BEMHelper from "react-bem-helper";

const classes = new BEMHelper("Image");

interface Props {
  src: string;
  alt?: string;
  maxWidth?: string;
  maxHeight?: string;
}

export type ImageType = FC<Props>;

const Image: ImageType = ({
  src,
  alt = "Image missing...",
  maxWidth = "auto",
  maxHeight = "auto",
}) => {
  return (
    <div {...classes("container")} style={{ maxWidth, maxHeight }}>
      <img src={src} alt={alt} />
    </div>
  );
};

export default Image;
