// Copyright by: P.J. Grochowski
import { MutableRefObject, useCallback, useRef } from "react";

type RefCleanupCallback = () => void;
type RefEffectCallback<T> = (element: T) => RefCleanupCallback | void;
type RefSetCallback<T> = (element: T | null) => void;

const useRefEffect = <T = HTMLElement>(
  callback: RefEffectCallback<T>
): [MutableRefObject<T | null>, RefSetCallback<T>] => {
  const ref = useRef<T | null>(null);
  const cleanup = useRef<RefCleanupCallback | null>(null);

  const setRef = useCallback(
    (element: T | null) => {
      if (element === ref.current) return;

      ref.current = element;
      cleanup.current?.();
      cleanup.current =
        element != null
          ? (callback(element) as RefCleanupCallback | null)
          : null;
    },
    [callback]
  );

  return [ref, setRef];
};

export default useRefEffect;
