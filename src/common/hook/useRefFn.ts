// Copyright by: P.J. Grochowski
import { MutableRefObject, useRef } from "react";

const SENTINEL = Symbol("SENTINEL");

const useRefFn = <T>(init: () => T): MutableRefObject<T> => {
  const ref = useRef<T | typeof SENTINEL>(SENTINEL);
  if (ref.current === SENTINEL) {
    ref.current = init();
  }
  return ref as MutableRefObject<T>;
};

export default useRefFn;
