// Copyright by: P.J. Grochowski
import { useEffect, useState } from "react";

export interface WindowSize {
  width: number;
  height: number;
}

const getWindowSize = (): WindowSize => ({
  width: window.innerWidth,
  height: window.innerHeight,
});

const useWindowSize = (): WindowSize => {
  const [size, setSize] = useState<WindowSize>(getWindowSize);

  useEffect(() => {
    const onResize = () => setSize(getWindowSize());

    window.addEventListener("resize", onResize);

    return () => window.removeEventListener("resize", onResize);
  }, []);

  return size;
};

export default useWindowSize;
