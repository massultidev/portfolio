// Copyright by: P.J. Grochowski
import { useEffect } from "react";
import { useServicesContext } from "./ServicesProvider";

export { default as DeviceTypeObserver } from "./DeviceTypeObserver";
export { default as FragmentRegistryService } from "./FragmentRegistryService";
export { default as IntersectionService } from "./IntersectionService";
export { default as OutsideClickObserver } from "./OutsideClickObserver";
export { default as PageScrollObserver } from "./PageScrollObserver";
export { default as ServicesProvider } from "./ServicesProvider";
export * from "./ServicesProvider";

export const useFragmentRegistration = (tagId: string, label: string) => {
  const { fragmentRegistryService } = useServicesContext();

  useEffect(() => {
    fragmentRegistryService.add(tagId, label);

    return () => fragmentRegistryService.remove(tagId);
  }, [tagId, label, fragmentRegistryService]);
};
