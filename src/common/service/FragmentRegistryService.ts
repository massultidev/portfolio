// Copyright by: P.J. Grochowski

type FragmentIdToNameMap = Map<string, string>;
type Callback = (fragments: FragmentIdToNameMap) => void;

export default class FragmentRegistryService {
  private callbacks = new Set<Callback>();
  private fragments = new Map<string, string>();

  public add(tagId: string, label: string) {
    if (this.fragments.has(tagId)) {
      throw Error(`Cannot add item by existing ID: '${tagId}'`);
    }
    this.fragments.set(tagId, label);
    this.update();
  }

  public remove(tagId: string) {
    if (!this.fragments.has(tagId)) {
      throw Error(`Cannot remove item by inexistent ID:  '${tagId}'`);
    }
    this.fragments.delete(tagId);
    this.update();
  }

  public subscribe(setterCallback: Callback) {
    this.callbacks.add(setterCallback);
  }

  public unsubscribe(setterCallback: Callback) {
    this.callbacks.delete(setterCallback);
  }

  private update() {
    this.callbacks.forEach((callback) => callback(this.fragments));
  }
}
