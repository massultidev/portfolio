// Copyright by: P.J. Grochowski
import breakpoint from "src/common/style/_breakpoints.module.scss";

export enum DeviceType {
  Mobile,
  Tablet,
  Desktop,
}

type Callback = (deviceType: DeviceType) => void;

const DEFAULT_DEVICE_TYPE = DeviceType.Desktop;

const deviceToWidthMap = new Map<DeviceType, number>([
  [DeviceType.Mobile, parseInt(breakpoint.MOBILE)],
  [DeviceType.Tablet, parseInt(breakpoint.TABLET)],
]);

export default class DeviceTypeObserver {
  private device: DeviceType;
  private callbacks = new Set<Callback>();

  constructor() {
    this.device = DeviceTypeObserver.findDeviceType();
    window.addEventListener("resize", () => this.onResize());
  }

  public get deviceType(): DeviceType {
    return this.device;
  }

  public subscribe(setterCallback: Callback) {
    this.callbacks.add(setterCallback);
  }

  public unsubscribe(setterCallback: Callback) {
    this.callbacks.delete(setterCallback);
  }

  private notify() {
    this.callbacks.forEach((callback) => callback(this.device));
  }

  private onResize() {
    const deviceType = DeviceTypeObserver.findDeviceType();
    if (deviceType !== this.device) {
      this.device = deviceType;
      this.notify();
    }
  }

  private static findDeviceType(): DeviceType {
    const size = Math.min(window.innerWidth, window.innerHeight);
    const entry = Array.from(deviceToWidthMap).find(
      ([key, value]) => size < value
    );
    return entry?.[0] ?? DEFAULT_DEVICE_TYPE;
  }
}
