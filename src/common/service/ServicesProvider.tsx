// Copyright by: P.J. Grochowski
import React, { FC, PropsWithChildren, useContext } from "react";
import DeviceTypeObserver from "./DeviceTypeObserver";
import FragmentRegistryService from "./FragmentRegistryService";
import OutsideClickObserver from "./OutsideClickObserver";
import PageScrollObserver from "./PageScrollObserver";

interface Services {
  fragmentRegistryService: FragmentRegistryService;
  deviceTypeObserver: DeviceTypeObserver;
  outsideClickObserver: OutsideClickObserver;
  pageScrollObserver: PageScrollObserver;
}

const services: Services = {
  fragmentRegistryService: new FragmentRegistryService(),
  deviceTypeObserver: new DeviceTypeObserver(),
  outsideClickObserver: new OutsideClickObserver(),
  pageScrollObserver: new PageScrollObserver(),
};

const ServicesContext = React.createContext<Services | null>(null);

export const useServicesContext = () => useContext(ServicesContext) as Services;

const ServicesProvider: FC<PropsWithChildren> = ({ children }) => {
  return (
    <ServicesContext.Provider value={services}>
      {children}
    </ServicesContext.Provider>
  );
};

export default ServicesProvider;
