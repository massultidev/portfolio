// Copyright by: P.J. Grochowski

type TargetSelector = string;
type Callback = () => void;

export default class OutsideClickObserver {
  private callbacks = new Map<TargetSelector, Callback>();

  constructor() {
    document.addEventListener("click", (event) => this.onClick(event));
  }

  public isSubscribed(selector: TargetSelector): boolean {
    return this.callbacks.has(selector);
  }

  public subscribe(selector: TargetSelector, callback: Callback) {
    if (this.isSubscribed(selector)) {
      throw Error(
        `Callback for selector already exists! Selector: '${selector}'`
      );
    }
    this.callbacks.set(selector, callback);
  }

  public unsubscribe(selector: TargetSelector) {
    if (!this.isSubscribed(selector)) {
      throw Error(
        `Callback for selector doesn't exist! Selector: '${selector}'`
      );
    }
    this.callbacks.delete(selector);
  }

  private onClick(event: MouseEvent) {
    Array.from(this.callbacks).forEach(([key, value]) => {
      const target = document.querySelector(key);
      if (target != null && !event.composedPath().includes(target)) {
        value();
      }
    });
  }
}
