// Copyright by: P.J. Grochowski

export enum ScrollDirection {
  Up,
  Down,
}

type Callback = (direction: ScrollDirection) => void;

export default class PageScrollObserver {
  private direction = ScrollDirection.Up;
  private prevYOffset = 0;
  private forceUpdate = false;

  private callbacks = new Set<Callback>();

  constructor() {
    document.addEventListener("scroll", () => this.onScroll());
  }

  public forceNextUpdate() {
    this.forceUpdate = true;
  }

  public subscribe(callback: Callback) {
    this.callbacks.add(callback);
  }

  public unsubscribe(callback: Callback) {
    this.callbacks.delete(callback);
  }

  private onScroll() {
    const offset = window.scrollY;
    const newDirection =
      offset > this.prevYOffset ? ScrollDirection.Down : ScrollDirection.Up;

    this.prevYOffset = offset;

    if (this.forceUpdate || newDirection !== this.direction) {
      this.forceUpdate = false;
      this.direction = newDirection;

      this.callbacks.forEach((callback) => callback(this.direction));
    }
  }
}
