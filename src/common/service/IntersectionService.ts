// Copyright by: P.J. Grochowski

export type IntersectionCallback = (
  entries: IntersectionObserverEntry[]
) => void;

export default class IntersectionService {
  private observer: IntersectionObserver | null = null;
  private subscribers = new Set<Element>();

  constructor(
    private readonly callback: IntersectionCallback,
    private readonly rootMargin: string = "",
    private readonly threshold: number | number[] = 1.0
  ) {}

  public set root(element: Element | Document | null) {
    this.observer?.disconnect();
    this.observer =
      element == null
        ? null
        : new IntersectionObserver(
            (entries, observer) => this.callback(entries),
            {
              root: element,
              rootMargin: this.rootMargin,
              threshold: this.threshold,
            }
          );

    if (this.observer != null) {
      this.subscribers.forEach((subscriber) =>
        this.observer?.observe(subscriber)
      );
    }
  }

  public isSubscribed(element: Element): boolean {
    return this.subscribers.has(element);
  }

  public subscribe(element: Element) {
    if (this.isSubscribed(element)) {
      console.error(element);
      throw Error("Cannot subscribe! Element already subscribed!");
    }
    this.subscribers.add(element);
    this.observer?.observe(element);
  }

  public unsubscribe(element: Element) {
    if (!this.isSubscribed(element)) {
      console.error(element);
      throw Error("Cannot unsubscribe! Element not subscribed!");
    }
    this.subscribers.delete(element);
    this.observer?.unobserve(element);
  }
}
